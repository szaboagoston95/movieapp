//
//  Movie.swift
//  MovieApp
//
//  Created by Szabó Ágoston on 2020. 03. 28..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import Foundation
import SwiftUI

struct Movie: Decodable, Identifiable {
    var id: Int
    var title: String
    var imagePath: String?
    var rating: Double
    var overView: String
    
    private enum CodingKeys: String, CodingKey {
        case imagePath = "poster_path"
        case id
        case title
        case rating = "vote_average"
        case overView = "overview"
        
    }
}

struct JsonResponse: Decodable {
    var results: [Movie]?
    var totalPages: Int
    
    private enum CodingKeys: String, CodingKey {
        case totalPages = "total_pages"
        case results
    }
}
