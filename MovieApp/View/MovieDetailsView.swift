//
//  MovieDetailsView.swift
//  MovieApp
//
//  Created by Szabó Ágoston on 2020. 03. 28..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import SwiftUI
import URLImage

struct MovieDetailsView: View {
    //    @ObservedObject var imageLoader: ImageLoader
    @ObservedObject var movieVM = MovieViewModel()
    
    let movieTitle: Movie
    
    var body: some View {
        ScrollView(){
        VStack(spacing: 0){
            ZStack{
                Color.gray.opacity(0.5).blur(radius: 5.0, opaque: false).edgesIgnoringSafeArea(.all)
            }.frame(height: 200)
            VStack(spacing: 15) {
                VStack {
                    URLImage(URL(string: "https://image.tmdb.org/t/p/w300\(movieTitle.imagePath ?? "")")!){imageProxy in
                        imageProxy.image.resizable()
                        
                    }
                    .frame(width: 200, height: 300)
                        //                    .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
                        .overlay(Rectangle().stroke(Color.white, lineWidth: 5))
                        .shadow(radius: 10)
                    //                    Image(uiImage: movie.image ?? UIImage(named: "movie")!)
                    
                    //                    .frame(width: 200, height: 300)
                    //                    .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
                    //                    .overlay(RoundedRectangle(cornerRadius: 20, style: .continuous).stroke(Color.white, lineWidth: 5))
                    //                        .shadow(radius: 10)
                }
                .background(Color.white)
                Text(movieTitle.title)
                    .font(.system(size: 30, weight: .bold, design: .rounded))
                    .lineLimit(nil)
                    .frame(maxWidth: .infinity, alignment: .center)
                    .fixedSize(horizontal: false, vertical: true)
                
                //                    VStack(spacing: 5) {
                //                    Text("Budget: \(movie.budget)$")
                //                        .font(.system(size: 20, weight: .semibold, design: .rounded))
                //                        .frame(maxWidth: .infinity, alignment: .leading)
                //                        .lineLimit(nil)
                Text("Rating: " + String(format: "%.01f", movieTitle.rating) + "/10")
                    .font(.system(size: 20, weight: .semibold, design: .rounded))
                    .frame(maxWidth: .infinity, alignment: .top)
                    .lineLimit(nil)
                
                
                    
                        Text(movieTitle.overView)
                            .font(.system(size: 20, weight: .regular, design: .rounded))
                            .frame(maxWidth: .infinity, alignment: .top)
                            .lineLimit(nil)
                        
                        
                        //                    }
                    
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
            .padding(.horizontal, 5)
            .offset(y: -150)
            }}.edgesIgnoringSafeArea(.top)
    }
}

struct MovieDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        MovieDetailsView(movieTitle: Movie.init(id: 1, title: "Star Wars", imagePath: "", rating: 8.6, overView: "Réges régen egy messzi messzi galaxisban..."))
    }
}


