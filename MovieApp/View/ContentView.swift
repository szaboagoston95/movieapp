//
//  ContentView.swift
//  MovieApp
//
//  Created by Szabó Ágoston on 2020. 03. 27..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    @ObservedObject var movieVM = MovieViewModel()
    
    @State private var search: String = ""
    @State var input: String = ""
    @State var typing = false
    @State var showSearchBar = true
    
    var body: some View {
        ZStack {
            VStack(spacing: 0){
                HStack {
                    TextField("Search", text: $input, onEditingChanged: {
                        self.typing = $0
                        let query = self.input.replacingOccurrences(of: " ", with: "+")
                        self.movieVM.search(keyWord: query)
                        
                    }, onCommit: {
                        
                    })
                        .frame(height: 50)
                        .padding(.horizontal, 10)
                        
                        .background(Color.gray.opacity(0.2))
                }
                .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous)).padding(.horizontal, 10)
                NavigationView {
                    if(movieVM.movies.count == 0) {
                        Text("No content")
                    } else {
                        List(movieVM.movies) { movieTitle in
                            NavigationLink(destination: MovieDetailsView(movieTitle: movieTitle)) {
                                Text(movieTitle.title)
                            }
                        }.navigationBarTitle(Text("Movies"))
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
