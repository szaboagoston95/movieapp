//
//  MovieViewModel.swift
//  MovieApp
//
//  Created by Szabó Ágoston on 2020. 03. 28..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import Foundation
import SwiftUI

let apiKey = "555dd34b51d2f5b7f9fdb39e04986933"

class MovieViewModel: ObservableObject {
    @Published var movies = [Movie]()
    
    let imageUrl = "https://image.tmdb.org/t/p/w500/"
    let apiQueryUrl = "https://api.themoviedb.org/3/search/movie?api_key=\(apiKey)"
    
    func search(keyWord: String) {
        var tempMovieIdList = [Movie]()
        if(keyWord == "") {
            DispatchQueue.main.async {
                self.movies = tempMovieIdList
            }
        }
        searchFirstPage(keyWord: keyWord) { jsonResponse in
            guard let jsonResponse = jsonResponse else { return }
            let pages = jsonResponse.totalPages
            guard let results = jsonResponse.results else { return }
            tempMovieIdList = results
            
            if(pages > 1) {
                
                for page in 2...pages {
                    
                    self.searchOtherPages(keyWord: keyWord, page: page) { movieIds in
                        tempMovieIdList.append(contentsOf: movieIds)
                        if(page == pages) {
                            DispatchQueue.main.async {
                                self.movies = tempMovieIdList
                            }
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.movies = tempMovieIdList
                }
            }
        }
    }
    
    func searchFirstPage(keyWord: String, completion: @escaping (_ response: JsonResponse?) -> ()) {
        print(keyWord)
        if(keyWord != "") {
            guard let url = URL(string: "\(apiQueryUrl)&query="+keyWord) else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard let data = data, error == nil else { return }
                let responseJson = try! JSONDecoder().decode(JsonResponse.self, from: data)
                
                let pages = responseJson.totalPages
                
                print("Oldalak: ", pages)
                
                completion(responseJson)
            }.resume()
        }
    }
    
    func searchOtherPages(keyWord: String, page: Int, completion: @escaping (_ movieIds: [Movie]) -> ()) {
        guard let url = URL(string: "\(apiQueryUrl)&page=\(page)&query=Star+Wars") else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let data = data, error == nil else { return }
        let pageResponse = try! JSONDecoder().decode(JsonResponse.self, from: data)
        guard let results = pageResponse.results else { return }
        completion(results)
            
        }.resume()
    }
}
